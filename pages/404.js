/** @format */

import Layout from "@/components/layout";
import Link from "next/link";

/** @format */
export default function Pagina404() {
    return (
        <Layout title="Página No Encontrada">
            <div className="error">
                <p> Página No Encontrada</p>
                <Link href="/"> Ir a Inicio</Link>
            </div>
        </Layout>
    );
}
