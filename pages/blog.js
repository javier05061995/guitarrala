/** @format */

import Layout from "@/components/layout";
import Post from "@/components/post";
import styles from "@/styles/grid.module.css";

const Blog = ({ posts }) => {
    console.log(posts);
    return (
        <div>
            <Layout title={"Blog "} description={"Blog de música, venta de guitarras, consejos,m GuitarLa"}>
                <main className="contenedor">
                    <h1 className="heading"> Blog</h1>
                    <div className={styles.grid}>
                        {posts?.map(post => (
                            <Post key={post.div} blog={post.attributes}></Post>
                        ))}
                    </div>
                </main>
            </Layout>
        </div>
    );
};

export default Blog;

export async function getStaticProps() {
    const repuesta = await fetch(`${process.env.API_URL}/posts?populate=imagen`);
    const { data: posts } = await repuesta.json();
    return {
        props: { posts },
    };
}
