/** @format */
import Layout from "@/components/layout";
import Image from "next/image";

import styles from "@/styles/nosotros.module.css";

const Nosotros = () => {
    return (
        <div>
            <Layout title={"Nosotros"} description={"Blog de musica, venta de guitarras"}>
                <main className="contenedor">
                    <h2 className="heading">Nosotros</h2>
                    <div className={styles.contenido}>
                        <Image src={"/img/nosotros.jpg"} alt="Imagen sobre nosotros" width={1000} height={800} />
                        <div>
                            <p>
                                Aenean ut nisl dui. Nulla ultrices leo nunc, vel tempor leo sollicitudin a. Ut nec
                                scelerisque nisl. Nunc blandit leo porta sem egestas blandit. Nullam finibus sodales
                                erat ut ultricies. Maecenas condimentum nisi at ex posuere, vel egestas sem facilisis.
                                Proin rutrum ultrices augue vitae convallis. Vivamus tristique, libero eget
                            </p>
                            <p>
                                Aliquam libero dui, gravida ut semper a, vestibulum id augue. Nunc vitae est a odio
                                aliquet condimentum. Ut sed vulputate turpis, nec tristique ligula. Orci varius amet,
                                fringilla porttitor dolor.
                            </p>
                        </div>
                    </div>
                </main>
            </Layout>
        </div>
    );
};

export default Nosotros;
