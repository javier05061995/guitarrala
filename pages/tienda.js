/** @format */

import Layout from "@/components/layout";
import Guitarras from "@/components/listado-guitarras";
import styles from "@/styles/grid.module.css";
const Tienda = ({ guitarras }) => {
    return (
        <div>
            <Layout title={"Tienda Virual"} description={"Tienda Virtual, venta de guitarras"}>
                <main className="contenedor">
                    <h1 className="heading">Nuestra Colección</h1>
                    <div className={styles.grid}>
                        {guitarras.map(guitarra => (
                            <Guitarras key={guitarra.id} guitarras={guitarra.attributes} />
                        ))}
                    </div>
                </main>
            </Layout>
        </div>
    );
};

export default Tienda;

// export async function getStaticProps() {
//     const repuesta = await fetch(`${process.env.API_URL}/guitaras?populate=imagen`);
//     const { data: guitarras } = await repuesta.json();
//     return {
//         props: { guitarras },
//     };
// }

export async function getServerSideProps() {
    const repuesta = await fetch(`${process.env.API_URL}/guitarras?populate=imagen`);
    const { data: guitarras } = await repuesta.json();
    return {
        props: { guitarras },
    };
}
