/** @format */
import Layout from "@/components/layout";
import Guitarras from "@/components/listado-guitarras";
import Post from "@/components/post";
import Curso from "@/components/curso";
import styles from "@/styles/grid.module.css";
/** @format */
export default function Home({ guitarras, posts, curso }) {
    return (
        <>
            <Layout title={"inicio"} description={"Sobre nosotros, guitarLA, tienda de música"}>
                <main className="contenedor">
                    <h1 className="heading">Nuestra Colección</h1>
                    <div className={styles.grid}>
                        {guitarras?.map(guitarra => (
                            <Guitarras key={guitarra.id} guitarras={guitarra.attributes} />
                        ))}
                    </div>
                </main>
                <Curso curso={curso.attributes} />

                <section className="contenedor">
                    <h1 className="heading">Blog</h1>
                    <div className={styles.grid}>
                        {posts?.map(posts => (
                            <Post key={posts.id} blog={posts.attributes} />
                        ))}
                    </div>
                </section>
            </Layout>
        </>
    );
}

export async function getStaticProps() {
    const urlGuitarras = `${process.env.API_URL}/guitarras?populate=imagen`;
    const urlPosts = `${process.env.API_URL}/posts?populate=imagen`;
    const urlCurso = `${process.env.API_URL}/curso?populate=imagen`;

    const [resGuitarras, resPosts, resCurso] = await Promise.all([
        fetch(urlGuitarras),
        fetch(urlPosts),
        fetch(urlCurso),
    ]);

    const [{ data: guitarras }, { data: posts }, { data: curso }] = await Promise.all([
        resGuitarras.json(),
        resPosts.json(),
        resCurso.json(),
    ]);

    return {
        props: {
            guitarras,
            posts,
            curso,
        },
    };
}
