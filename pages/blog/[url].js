/** @format */
import Image from "next/image";
import Layout from "@/components/layout";
import styles from "@/styles/blog.module.css";
import { formatearFecha } from "@/utils/helpers";

export default function Blog({ blog }) {
    const { contenido, imagen, titulo, publishedAt } = blog[0].attributes;
    console.log(blog);

    return (
        <Layout title={titulo}>
            <article className={`${styles.post} ${styles["mt-3"]}`}>
                <Image src={imagen.data.attributes.url} alt={`imagen blog ${titulo}`} width={1000} height={400} />
                <div>
                    <h3>{titulo}</h3>
                    <p className={styles.fecha}>{formatearFecha(publishedAt)}</p>
                    <p className={styles.texto}> {contenido[0].children[0].text}</p>
                </div>
            </article>
        </Layout>
    );
}

// export async function getStaticPaths() {
//     const respuesta = await fetch(`${process.env.API_URL}/post`);
//     const { data } = await respuesta.json();
//     const paths = data.map(blog => ({
//         params: {
//             url: blog.attributes.url,
//         },
//     }));

//     return {
//         paths,
//         fallback: false,
//     };
// }

// export async function getStaticProps({ params: { url } }) {
//     const res = await fetch(`${process.env.API_URL}/post?filters[url]=${url}&populate=imagen`);
//     const { data: blog } = await res.json();
//     return {
//         props: {
//             blog,
//         },
//     };
export async function getServerSideProps({ query: { url } }) {
    const res = await fetch(`${process.env.API_URL}/posts?filters[url]=${url}&populate=imagen`);
    const { data: blog } = await res.json();
    console.log(blog);
    return {
        props: {
            blog,
        },
    };
}
