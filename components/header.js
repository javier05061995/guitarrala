/** @format */
import Image from "next/image";
import Link from "next/link";
import styles from "../styles/header.module.css";
import { useRouter } from "next/router";

export default function Header() {
    const router = useRouter();
    return (
        <header className={styles.header}>
            <div className={`contenedor ${styles.barra}`}>
                <Link href={"/"}>
                    <Image src={"/img/logo.svg"} width={300} height={40} alt="imagen logoTipo" />
                </Link>
                <nav className={styles.navegacion}>
                    <Link href="/">
                        <div className={router.pathname === "/" ? styles.active : ""}>Inicio</div>
                    </Link>
                    <Link href="/nosotros">
                        <div className={router.pathname === "/nosotros" ? styles.active : ""}>Nosotros</div>
                    </Link>
                    <Link href="/tienda">
                        <div className={router.pathname === "/tienda" ? styles.active : ""}>Tienda</div>
                    </Link>
                    <Link href="/blog">
                        <div className={router.pathname === "/blog" ? styles.active : ""}>Blog</div>
                    </Link>
                    <Link href="/carrito">
                        <div className={router.pathname === "/carrito" ? styles.active : ""}>
                            <Image src={"/img/carrito.png"} width={30} height={25} alt="imagen Carrito" />
                        </div>
                    </Link>
                </nav>
            </div>
        </header>
    );
}
