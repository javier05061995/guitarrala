/** @format */
import Image from "next/image";
import Link from "next/link";
import styles from "@/styles/blog.module.css";
import { formatearFecha } from "@/utils/helpers";

export default function Post({ blog }) {
    const { contenido, imagen, titulo, url, publishedAt } = blog;
    return (
        <article className={styles.post}>
            <Image
                src={imagen.data.attributes.formats.medium.url}
                alt={`imagen blog ${titulo}`}
                width={600}
                height={400}
            />
            <div>
                <h4>{titulo}</h4>
                <p className={styles.fecha}>{formatearFecha(publishedAt)}</p>
                <p className={styles.resumen}> {contenido[0].children[0].text}</p>
                <Link href={`/blog/${url}`}> LEER POST</Link>
            </div>
        </article>
    );
}
